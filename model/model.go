package model

import "time"

type Todo struct {
	ID uint 			`gorm:"not null;primaryKey;autoIncrement"`
	Description string 	`gorm:"not null"`
	CreatedAt time.Time
	UpdatedAt time.Time
}