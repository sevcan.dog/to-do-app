package dto

type TodoDTO struct {
	Id uint 		   `json:"id"`
	Description string `json:"description" binding:"required"`
}