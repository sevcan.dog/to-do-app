package util

import (
	"os"
	"testing"

	"github.com/joho/godotenv"
	"github.com/stretchr/testify/assert"
)

func Setup(t *testing.T) {
	godotenv.Load("../.env")
	os.Setenv("ENV_MODE", "DEBUG")
	assert.FileExists(t, "../.env")
}

func TearDown(t *testing.T) {
	godotenv.Load("../.env")
	os.Setenv("ENV_MODE", "RELEASE")
	assert.FileExists(t, "../.env")
}