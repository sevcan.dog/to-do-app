FROM golang:1.17.2 AS build

WORKDIR /app

# add app
COPY . ./

# download the required Go dependencies
RUN go mod download


# show files 
RUN ls

RUN go build -o /todo-app-api .

EXPOSE 8000

CMD [ "/todo-app-api" ]