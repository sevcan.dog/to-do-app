package service

import (
	"gitlab.com/sevcan.dog/to-do-app/model"
	"gitlab.com/sevcan.dog/to-do-app/repository"
)
 
func CreateTodo(todo *model.Todo) error {

	repository.SetRepository()

	err := repository.InsertTodo(todo)

	return err
}

func GetTodos() ([]model.Todo, error) {

	repository.SetRepository()

	todos, err := repository.GetTodos()

	return todos, err
}