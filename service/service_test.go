package service

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/sevcan.dog/to-do-app/model"
	"gitlab.com/sevcan.dog/to-do-app/util"
)
 
 func TestCreateTodo(t *testing.T) {

	util.Setup(t)

	// create a Todo
	todo := model.Todo{Description: "Buy smt"}
	
	err := CreateTodo(&todo)

	if err != nil {
		t.Errorf("Cannot add todo into memory !")
	}

	util.TearDown(t)
}


func TestGetTodos(t *testing.T) {

	util.Setup(t)
	
	todos , err := GetTodos()

	if err != nil {
		t.Errorf("Cannot get todos into memory !")
	}

	assert.NotNil(t, todos)

	util.TearDown(t)
}