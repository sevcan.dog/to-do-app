package main

import (
   "os"
	"testing"

	"github.com/pact-foundation/pact-go/dsl"
	"github.com/pact-foundation/pact-go/types"
   "github.com/joho/godotenv"
)

func loadEnv() {
   godotenv.Load()
}

func TestProvider(t *testing.T) {

   loadEnv()
   
   pact := dsl.Pact{
      Consumer:                 "todo-app-ui",
      Provider:                 "todo-app-api",
      DisableToolValidityCheck: true,
      LogLevel:                 "INFO",
   }
   
   _, err := pact.VerifyProvider(t, types.VerifyRequest{
      ProviderBaseURL:            os.Getenv("PROVIDER_BASE_URL"),
      BrokerURL:                  os.Getenv("PACT_BROKER_URL"),
      BrokerToken:                os.Getenv("PACT_BROKER_TOKEN"),
      PublishVerificationResults: true,
      ProviderVersion:            "1.0.0",
   })

   if err != nil {
      t.Fatal(err)
   }
}
  
