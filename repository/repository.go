package repository

import (
	_ "fmt"
	"os"

	"gitlab.com/sevcan.dog/to-do-app/model"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var commonDB *gorm.DB

func getDbConfig() *gorm.Config {

	env := os.Getenv("ENV_MODE")

	if env == "DEBUG" {
		return &gorm.Config{
					Logger: logger.Default.LogMode(logger.Info),
				}
	} else {
		return &gorm.Config{}
	}
}

func SetRepository() error {

	dbName, _ := os.LookupEnv(os.Getenv("ENV_MODE")+"_DB_NAME") 
	db, err := gorm.Open(sqlite.Open(dbName), getDbConfig())

	if err != nil {
		return err
	}

	commonDB = db
	commonDB.AutoMigrate(&model.Todo{})

	return nil
}

func InsertTodo(todo *model.Todo) error {

	transactionResult := commonDB.Create(todo)
	if err := transactionResult.Error; err != nil {
		return err
	}
	return nil
}

func GetTodos() ([]model.Todo, error) {

	var todosFromDB []model.Todo
	transactionResult := commonDB.Find(&todosFromDB)

	if err := transactionResult.Error; err != nil {
		return todosFromDB, err
	}
	return todosFromDB, nil
}