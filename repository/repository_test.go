package repository

import (
	"fmt"
	"os"
	"testing"

	mocket "github.com/selvatico/go-mocket"
	"github.com/stretchr/testify/assert"
	"gitlab.com/sevcan.dog/to-do-app/model"
	"gitlab.com/sevcan.dog/to-do-app/util"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

func TestGetDbConfig(t *testing.T) {

	os.Setenv("ENV_MODE", "RELEASE")
	dbConfig := getDbConfig()

	assert.Equal(t, dbConfig, &gorm.Config{})

	os.Setenv("ENV_MODE", "DEBUG")
	dbConfig = getDbConfig()

	assert.Equal(t, dbConfig, &gorm.Config{
									Logger: logger.Default.LogMode(logger.Info),
								})

	util.TearDown(t)
}


func SetupTests(t *testing.T) {

	util.Setup(t)

	mocket.Catcher.Register()
	mocket.Catcher.Logging = true
	
	err :=  SetRepository()

	if err != nil {
		t.Errorf("Error while creating the database")
	}
}

func TestInsertTodoToDb(t *testing.T) {
	SetupTests(t)

	mocket.Catcher.Logging = true
	mocket.Catcher.Reset().NewMock().WithQuery(`INSERT INTO todos VALUES`).WithQueryException()

	todo := model.Todo{Description: "Buy smt"}
	err := InsertTodo(&todo)

	fmt.Println("TODO:", todo)

	if err != nil {
		t.Errorf("Cannot insert the todo into the database")
	}

	util.TearDown(t)
}

func TestInsertMultipleTodoToDb(t *testing.T) {
	SetupTests(t)

	mocket.Catcher.Logging = true
	mocket.Catcher.Reset().NewMock().WithQuery(`INSERT INTO todos VALUES`).WithQueryException()

	todos := []model.Todo{{Description: "Buy smt"}, {Description: "Buy smt2"}}
	
	for _,todo := range todos {
		err := InsertTodo(&todo)

		if err != nil {
			t.Errorf("Cannot insert the todos into the database")
		}
	}

	util.TearDown(t)
}

func TestGetTodosFromDb(t *testing.T) {
	SetupTests(t)

	mocket.Catcher.Logging = true
	mocket.Catcher.Reset().NewMock().WithQuery(`SELECT * FROM todos`).WithQueryException()

	// insert a todo to get
	todo := model.Todo{Description: "Order smt"}
	err := InsertTodo(&todo)
	if err != nil {
		t.Errorf("Cannot insert the todo to get after")
	}

	todos, err := GetTodos()
	if err != nil {
		t.Errorf("Cannot get the todos")
	}

	assert.NotNil(t, todos)

	// check if the inserted todo returned in todos
	takenTodo := todos[0]
	assert.Equal(t, takenTodo.ID, todo.ID)
	assert.Equal(t, takenTodo.Description, todo.Description)

	util.TearDown(t)
}