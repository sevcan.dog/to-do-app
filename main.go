package main

import (
	"github.com/joho/godotenv"
	"gitlab.com/sevcan.dog/to-do-app/controller"
)

func main() {
	godotenv.Load()
	controller.InitializeController()
}
