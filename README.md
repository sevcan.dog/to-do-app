# Todo App API

This project uses:
* [Gin-Gonic](https://github.com/gin-gonic/gin) as the web framework 
* [GORM](https://gorm.io/) as the ORM library
* SQlite3 as the database
* [testify](https://github.com/stretchr/testify) for testing
* [go-mocket](https://github.com/Selvatico/go-mocket) for testing GORM
* [net/http/httptest](https://pkg.go.dev/net/http/httptest) for HTTP testing 
* [godotenv](https://github.com/joho/godotenv) for manipulating environment variables

API documentation is [here](https://gitlab.com/sevcan.dog/to-do-app/-/blob/master/API.md).
```
    .
    ├── controller
    │   ├── controller.go
    │   └── controller_test.go
    ├── dto  
    │   └── dto.go
    ├── mapper  
    │   ├── mapper.go
    │   └── mapper_test.go
    ├── model  
    │   └── model.go
    ├── repository   
    │   ├── repository.go
    │   └── repository_test.go
    ├── service     
    │   ├── service.go
    │   └── service_test.go
    ├── util  
    │   └── test_util.go --> includes Setup&Teardown methods for preparing debug environment
    ├── .env   
    ├── .gitignore  
    ├── .gitlab-ci.yml --> includes pipeline configuration    
    ├── api-deployment.yaml   
    ├── api-service.yaml    
    ├── API.md 
    ├── db-persistent-volume-claim.yaml   
    ├── DockerFile     
    ├── go.mod  
    ├── go.sum 
    ├── main.go
    ├── pact_test.go --> includes Pact verification test
    ├── README.md
    └── todo-app-data --> includes todo data
```

## Usage

### With Docker:

→ **Requirements**
* Docker

1. Run `docker build -t todo-app-api .`
2. Run `docker run -dp 8000:8000 todo-app-api`
3. Route to [localhost:8000/todos](http://localhost:8000/todos)

### Without Docker:

→ **Requirements**
* Go (developed with  go1.17.1)
* Node.js (developed with v14.15.3)
* SQlite3

#### Test:

- Route to each folder and run `go test`to run all tests
- Before running `pact_test`: either configure the .env file or export the variables that needed to be configured 

### Run in debug mode:
- Run `go run main.go` to run the application in debug mode

### Build and Run in release mode:

1. Run `go build .` to build the application
2. Set export GIN_MODE=release
3. Run `./to-do-app` to run the application

### To use Gitlab CI:

- Create the following variables in PROTECTED mode

| Name            | Value                    | Environment |
|-----------------|--------------------------|-------------|
| BROKER_TOKEN    | _YOUR_PACT_BROKER_TOKEN_ | test        |
| BROKER_URL      | _YOUR_PACT_BROKER_URL_   | test        |
| PROVIDER_URL    | _YOUR_TEST_API_URL_      | test        |
| SERVICE_ACCOUNT | _YOUR_GCLOUD_SA_KEY_     | test        |
