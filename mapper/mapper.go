package mapper

import (
	"gitlab.com/sevcan.dog/to-do-app/dto"
	"gitlab.com/sevcan.dog/to-do-app/model"
)

func MapTodoDTOToTodo(todoDTO dto.TodoDTO) model.Todo {
	var todo model.Todo
	todo.Description = todoDTO.Description
	return todo
}

func MapTodoToTodoDTO(todo model.Todo) dto.TodoDTO {
	var todoDTO dto.TodoDTO
	todoDTO.Id = todo.ID
	todoDTO.Description = todo.Description
	return todoDTO
}

func MapTodosToTodoDTOs(todos []model.Todo) []dto.TodoDTO {
	var todoDTOs []dto.TodoDTO = []dto.TodoDTO{}
	
	for _, todo := range todos {
		todoDTOs = append(todoDTOs, MapTodoToTodoDTO(todo))
	}

	return todoDTOs
}