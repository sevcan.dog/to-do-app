package mapper

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/sevcan.dog/to-do-app/dto"
	"gitlab.com/sevcan.dog/to-do-app/model"
)

func TestMapTodoDTOToTodo(t *testing.T) {

	todoDTO := dto.TodoDTO{Description: "Call the father"}
	todo := MapTodoDTOToTodo(todoDTO)

	assert.Equal(t, todoDTO.Description, todo.Description)
}

func TestMapTodoToTodoDTO(t *testing.T) {

	todo := model.Todo{ID: 1, Description: "Call the father"}
	todoDTO := MapTodoToTodoDTO(todo)

	assert.Equal(t, todo.ID, todoDTO.Id)
	assert.Equal(t, todo.Description, todoDTO.Description)
}

func TestMapTodosToTodoDTOs(t *testing.T) {

	todos := []model.Todo{{ID: 1, Description: "Call the father"}, {ID: 2, Description: "Call the mother"}}
	todoDTOs := MapTodosToTodoDTOs(todos)

	for i, todoDTO := range todoDTOs {
		assert.Equal(t, todos[i].ID, todoDTO.Id)
		assert.Equal(t, todos[i].Description, todoDTO.Description)
	}
}

func TestMapEmptyTodosToTodoDTOs(t *testing.T) {

	todos := []model.Todo{}
	todoDTOs := MapTodosToTodoDTOs(todos)

	assert.NotNil(t, todoDTOs)
}