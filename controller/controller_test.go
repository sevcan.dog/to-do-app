package controller

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/sevcan.dog/to-do-app/dto"
	"gitlab.com/sevcan.dog/to-do-app/util"
)

func TestPostTodoWithValidTodo(t *testing.T) {

	util.Setup(t)

	router := setupController()
	resWriter := httptest.NewRecorder()

	// send post request
	todoJsonData,_ :=  json.Marshal(dto.TodoDTO{ Description: "Buy groceries" })
	req, _ := http.NewRequest("POST", "/todo", bytes.NewBuffer(todoJsonData))
	router.ServeHTTP(resWriter, req)

	// check response status code
	assert.Equal(t, 200, resWriter.Code)

	// check if id is returned properly
	var todoDTO dto.TodoDTO
	if err := json.Unmarshal([]byte(resWriter.Body.Bytes()), &todoDTO); err != nil{
		t.Errorf(err.Error())
	}
	assert.Equal(t, uint(1), todoDTO.Id)
	assert.Equal(t, "Buy groceries", todoDTO.Description)

	util.TearDown(t)
}

func TestPostTodoWithInvalidTodo(t *testing.T) {

	util.Setup(t)

	router := setupController()
	resWriter := httptest.NewRecorder()

	var invalidTodo struct{}
	todoJsonData,_ :=  json.Marshal(invalidTodo)
	req, _ := http.NewRequest("POST", "/todo", bytes.NewBuffer(todoJsonData))

	router.ServeHTTP(resWriter, req)

	assert.Equal(t, 400, resWriter.Code)

	util.TearDown(t)
}
func TestPostTodoWithEmptyTodo(t *testing.T) {

	util.Setup(t)

	router := setupController()
	resWriter := httptest.NewRecorder()

	// send post request
	req, _ := http.NewRequest("POST", "/todo", nil)
	router.ServeHTTP(resWriter, req)

	assert.Equal(t, 400, resWriter.Code)

	util.TearDown(t)
}

func TestGetTodos(t *testing.T) {

	util.Setup(t)

	router := setupController()
	resWriter := httptest.NewRecorder()

	// send post request
	req, _ := http.NewRequest("GET", "/todos", nil)
	router.ServeHTTP(resWriter, req)

	assert.Equal(t, 200, resWriter.Code)

	// check if todos is returned properly
	var todoDTOs []dto.TodoDTO
	if err := json.Unmarshal([]byte(resWriter.Body.Bytes()), &todoDTOs); err != nil{
		t.Errorf(err.Error())
	}
	assert.NotNil(t, todoDTOs)

	util.TearDown(t)
}