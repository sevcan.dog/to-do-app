package controller

import (
	"os"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gitlab.com/sevcan.dog/to-do-app/dto"
	"gitlab.com/sevcan.dog/to-do-app/mapper"
	"gitlab.com/sevcan.dog/to-do-app/service"
)

func setupController() *gin.Engine{

	router := gin.Default()
	router.Use(cors.Default())
	router.GET("/todos", GetTodos)
	router.POST("/todo", CreateTodo)
	return router
}

func InitializeController() {
	router := setupController()
	router.Run(os.Getenv("PORT"))
}

func CreateTodo(context *gin.Context) {

	var todoDTO dto.TodoDTO

	context.Header("Access-Control-Allow-Origin", "*")

	if err := context.BindJSON(&todoDTO); err != nil {
		context.JSON(400, gin.H{ "error": "invalid input" })
	} else {
		todo := mapper.MapTodoDTOToTodo(todoDTO)
		err = service.CreateTodo(&todo)
		todoDTO := mapper.MapTodoToTodoDTO(todo)

		if err != nil {
			context.JSON(500, gin.H{ "error": "internal server error" })
		} else {
			context.JSON(200, todoDTO)
		}
	}
}

func GetTodos(context *gin.Context) {

	context.Header("Access-Control-Allow-Origin", "*")

	todos, err := service.GetTodos()

	if err != nil {
		context.JSON(500, gin.H{ "error": "internal server error" })
	} else {
		todoDTOs := mapper.MapTodosToTodoDTOs(todos)
		context.JSON(200, todoDTOs)
	}
}
  