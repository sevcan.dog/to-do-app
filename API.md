
# Todo App REST API

## Get Todos
`GET` /todos

Use this endpoint to get all todos.

**Request Sample**  
`GET http://34.96.238.33:8000/todos`

**Response Sample**
 ```
 HTTP/1.1  200 OK Content-Type: application/json
[
	{
		"id": 1,
		"description": "Wash the car"
	},
	{
		"id": 2,
		"description": "Call the officer"
	}
]
```

## Create Todo
`POST` /todo

Use this endpoint to create a new todo.

<ins>**Request Data**</ins>
| Parameter     | Description            |
| :---          | :---                   
| description   | Todo description       | 

**Request Sample**  
`POST http://34.96.238.33:8000/todo`
```json
{
	"description": "Wash the car"
}
```
**Response Sample**
``` 
HTTP/1.1  200 OK Content-Type: application/json
{
	"id": 1,
	"description": "Wash the car"
}
```

## Error Responses
Error messages are accessible through **error** field of the response.

**Error Response Sample** 
```
HTTP/1.1  400 Bad Request 
{
	"error": "invalid input"
}
```